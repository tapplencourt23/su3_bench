#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <chrono>
typedef std::chrono::system_clock Clock;

#ifndef ITERATIONS
#  define ITERATIONS 100
#endif
#ifndef LDIM
#  define LDIM 32       // Lattice size = LDIM^4
#endif
#ifndef PRECISION
#  define PRECISION 2  // 1->single, 2->double
#endif

// Global variables
unsigned int verbose=1;
// global argc and argv for parsing model specific parameters 
int  g_argc;
char **g_argv;

#include "lattice.hpp"

#ifdef USE_CUDA
  #include <thrust/host_vector.h>
  #include "mat_nn_cuda.hpp"
#elif  USE_OPENMP
  #include "mat_nn_openmp.hpp"
#elif  USE_OPENACC
  #include "mat_nn_openacc.hpp"
#elif  USE_OPENCL
  // OpenCL 1.2 doesn't support complex data types
  #define MILC_COMPLEX
  #include "mat_nn_opencl.hpp"
#elif USE_SYCL
  #ifndef LINEAR
    #include "mat_nn_sycl.hpp"
  #else
    #include "mat_nn_sycl_linearize.hpp"
  #endif
#else
  #error Unknown programming model
#endif

#include <cassert>
template<class T>
bool almost_equal(T x, T y, double tol)
{
    return std::abs( x - y ) < tol ;
}

#ifdef RANDOM_INIT
#include <random>
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.f, 1.f);
#endif

// initializes su3_matrix to a given value
void init_link(su3_matrix *s, Complx val) {
  for(int j=0; j<4; ++j) for(int k=0; k<3; ++k) for(int l=0; l<3; ++l) {
#ifndef RANDOM_INIT
    s[j].e[k][l] = val;
#elif !defined MILC_COMPLEX
    s[j].e[k][l]=dis(gen);
#else
    s[j].e[k][l].real=dis(gen);
    s[j].e[k][l].imag=dis(gen);
#endif
  }
}

// initializes a lattice site 
void make_lattice(site *s, size_t n, Complx val) {
  int nx=n;
  int ny=n;
  int nz=n;
  int nt=n;
  for(int t=0;t<nt;t++) {
    int i=t*nz*ny*nx;
    for(int z=0;z<nz;z++)for(int y=0;y<ny;y++)for(int x=0;x<nx;x++,i++){
      s[i].x=x; s[i].y=y; s[i].z=z; s[i].t=t;
      s[i].index = x+nx*(y+ny*(z+nz*t));
      if( (x+y+z+t)%2 == 0)
        s[i].parity=EVEN;
      else
        s[i].parity=ODD;
      init_link(&s[i].link[0], val);
    }
  }
}

// Main
int main(int argc, char **argv)
{
  int opt;
  size_t iterations = ITERATIONS;
  size_t ldim = LDIM;
  size_t threads_per_group = 0;
  int device = -1;   // Use a default selector

  g_argc = argc;
  g_argv = argv;
  // parse command line for parameters
  while ((opt=getopt(argc, argv, ":hi:l:t:v:d:")) != -1) {
    switch (opt) {
    case 'i':
      iterations = atoi(optarg);
      break;
    case 'l':
      ldim = atoi(optarg);
      break;
    case 't':
      threads_per_group = atoi(optarg);
      break;
    case 'd':
      device = atoi(optarg);
      break;
    case 'v':
      verbose = atoi(optarg);
      break;
    case 'h':
      fprintf(stderr, "Usage: %s [-i iterations] [-l lattice dimension] \
[-t threads per workgroup] [-v verbosity level [0,1,2,3]]\n", argv[0]);
      exit (1);
    }
  }

  // allocate and initialize the working lattices and B su3 matrices
  size_t total_sites = ldim*ldim*ldim*ldim;
#ifndef USE_CUDA
  std::vector<site> a(total_sites);
  std::vector<su3_matrix> b(4);
  std::vector<site> c(total_sites);
#else
  thrust::host_vector<site> a(total_sites);
  thrust::host_vector<su3_matrix> b(4);
  thrust::host_vector<site> c(total_sites);
#endif

  // initialize the lattices
  make_lattice(a.data(), ldim, Complx{1.0,0.0});
  init_link(b.data(), Complx{1.0/3.0,0.0});

  if (verbose >= 1) {
    printf("Number of sites = %zu^4\n", ldim);
    printf("Executing %zu iterations\n", iterations);
    if (threads_per_group != 0)
      printf("Threads per group = %zu\n", threads_per_group);
  }

  // benchmark call
  const double ttotal = su3_mat_nn(a, b, c, total_sites, iterations, threads_per_group, device);
  if (verbose >= 1)
    printf("Total execution time = %f secs\n", ttotal);
  // calculate flops/s, etc.
  // each matrix multiply is (3*3)*4*(12 mult + 12 add) = 4*(108 mult + 108 add) = 4*216 ops
  const double tflop = (double)iterations * total_sites * 864.0;
  printf("Total GFLOP/s = %.3f\n", tflop / ttotal / 1.0e9);

  const double memory_usage = (double)sizeof(site)*(a.capacity()+c.capacity())+sizeof(su3_matrix)*b.capacity();
  printf("Total GByte/s (GPU memory)  = %.3f\n", iterations * memory_usage / ttotal / 1.0e9);

  // Verification of the result
  // If too expensive, we can just verify the sum
  // but I'm paranoid, so I verify each element
  for (int i=0;i<total_sites;++i) for(int j=0;j<4;++j)  for(int k=0;k<3;++k)  for(int l=0;l<3;++l) {
        Complx cc = {0.0, 0.0};
        for(int m=0;m<3;m++) {
          #ifdef MILC_COMPLEX
            CMULSUM( a[i].link[j].e[k][m], b[j].e[m][l], cc)
          #else
            cc += a[i].link[j].e[k][m] * b[j].e[m][l];
          #endif
        }
        
        #ifdef MILC_COMPLEX
           assert(almost_equal(c[i].link[j].e[k][l].real, cc.real, 1E-6));
           assert(almost_equal(c[i].link[j].e[k][l].imag, cc.imag, 1E-6));
        #else
          assert(almost_equal(c[i].link[j].e[k][l], cc, 1E-6));
        #endif     
  }

  // check memory usage
  if (verbose >= 2) {
    printf("Total allocation for matrices = %.3f MiB\n", memory_usage / 1048576.0);
    struct rusage usage;
    if (getrusage(RUSAGE_SELF, &usage) == 0)
      printf("Approximate memory usage = %.3f MiB\n", (float)usage.ru_maxrss/1024.0);
  }
}

